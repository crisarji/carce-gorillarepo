import { Component, OnInit  } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  {

	proportionalPercentage = 0;

	//This possibly comes from a service
	currentAmount = 56;
	targetAmount = 125;
	remainingAmount = 69;

	public ngOnInit()
	{
		this.getProportionalPercentage();
		var that = this;

		$(document).on('click', '.btn-outline-secondary', function () {
			$(".progress-bar").animate({
				width: `${that.proportionalPercentage}` + '%'
			}, 2500);
			$(".animation-line").find('.arrow-up').animate({
				left: `${that.proportionalPercentage}` + '%'
			}, 2500);
		});
	}

	getProportionalPercentage() {
		this.proportionalPercentage = this.currentAmount * 100 / this.targetAmount;
	}
}
