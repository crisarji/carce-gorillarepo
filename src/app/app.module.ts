import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';
import { AppComponent } from './app.component';

@Component({
  selector: 'my-app',
  template: `
  	<h1>Cristian Arce</h1>
  	<h2>- Progress Bar on Popover test -</h2>
  	<h3>Gorilla Logic</h3>
    <div class="container-fluid">
    <hr>
    <p>
      Create a web page that demonstrates the progress indicator feature 
      and paste a publicly accessible URL that demonstrates that feature
      into the answer box. <br>
      Also, list the questions that you would have asked
      the product manager had she been available prior to the demo
      and describe any decisions you had to make to finish the demo.
    </p>
    <hr>
    <app-root></app-root>
  </div>
  `
})
export class App {
}   

@NgModule({
  imports: [BrowserModule, FormsModule, ReactiveFormsModule, JsonpModule, NgbModule.forRoot()], 
  declarations: [App, AppComponent],
  bootstrap: [App]
}) 
export class AppModule {}
